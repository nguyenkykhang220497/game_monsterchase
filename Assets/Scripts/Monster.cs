﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    // HideInInspector truyen value cho Variable
    [HideInInspector]
    public float speed;

    Rigidbody2D m_rb;

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        m_rb.velocity = new Vector2(speed , m_rb.velocity.y);
    }
}
