﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour

{
    [SerializeField]
     float moveFocus = 10f;
    [SerializeField]
     float jumpFocus = 11f;
    //public float maxVelocity = 22f;

    Rigidbody2D m_rb;
    SpriteRenderer m_sp;
    Animator m_anim;
    string WALK_ANIMATION = "Walk";
    string GROUND_TAG = "Ground";
    string ENEMY_TAG = "Enemy";
    float movementX;
    Vector3 sceenLimit;
    bool m_isGround;

    private void Awake()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_sp = GetComponent<SpriteRenderer>();
        m_anim = GetComponent<Animator>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMoveKeyBoard();
        AnimPlayer();
    }

    private void FixedUpdate()
    {
        PlayerJump();
    }

    void PlayerMoveKeyBoard()
    {
        movementX = Input.GetAxisRaw("Horizontal");

        sceenLimit = transform.position + new Vector3(movementX, 0f, 0f) * Time.deltaTime * moveFocus;
        if (sceenLimit.x > 82 || sceenLimit.x < -82) return;
        transform.position = sceenLimit;


    }

    void AnimPlayer()
    {
        if(movementX < 0)
        {
            m_anim.SetBool(WALK_ANIMATION, true);
            m_sp.flipX = true;
        } else if (movementX > 0)
        {
            m_anim.SetBool(WALK_ANIMATION, true);
            m_sp.flipX = false;
        } else
        {
            m_anim.SetBool(WALK_ANIMATION, false);
        }
    }

    void PlayerJump()
    {
        if(Input.GetButtonDown("Jump") && m_isGround)
        {
            m_isGround = false;
            m_rb.AddForce(new Vector2(0f, jumpFocus), ForceMode2D.Impulse);
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.CompareTag(GROUND_TAG))
        {
            m_isGround = true;
        }
        if (col.gameObject.CompareTag(ENEMY_TAG))
        {
            Destroy(gameObject);
        }

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag(ENEMY_TAG))
        {
            Destroy(gameObject);
        }
    }
}
