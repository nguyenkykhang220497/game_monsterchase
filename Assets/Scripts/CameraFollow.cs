﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    Transform player;
    Vector3 temp;

    [SerializeField]
    float minX, maxX;
    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<Player>().transform;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void LateUpdate()
    {
        if (!player) return;
       
        temp = transform.position;
        temp.x = player.position.x;
        if (temp.x < minX) temp.x = minX;
        if (temp.x > maxX) temp.x = maxX;
        transform.position = temp;
    }
}
