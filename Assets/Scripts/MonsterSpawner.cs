﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    [SerializeField]
    GameObject[] monsterList;

    GameObject spawnMonster;

    [SerializeField]
    Transform leftPos, rightPos;

    int randomIndex;
    int randomSide;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnMonsters());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnMonsters()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(1, 5));

            randomIndex = Random.Range(0, monsterList.Length);
            randomSide = Random.Range(0, 2);

            //Debug.Log(rightPos.position);
            spawnMonster = Instantiate(monsterList[randomIndex]);

            if (randomSide == 0)
            {
                // left side
                spawnMonster.transform.position = leftPos.position;
                spawnMonster.GetComponent<Monster>().speed = Random.Range(4, 10);

            } else {
                // right side
                spawnMonster.transform.position = rightPos.position;
                spawnMonster.GetComponent<Monster>().speed = -Random.Range(4, 10);
                spawnMonster.transform.localScale = new Vector3(-1f, 1f, 1f);
            }

        }

    }
}
