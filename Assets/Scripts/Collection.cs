﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collection : MonoBehaviour
{
    string ENEMY_TAG = "Enemy";
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if( col.CompareTag(ENEMY_TAG))
        {
            Destroy(col.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.CompareTag(ENEMY_TAG))
        {
            Destroy(col.gameObject);
        }
    }
}
